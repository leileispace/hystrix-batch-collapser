package com.leilei.controller;

import com.leilei.entity.AlarmDictionary;
import com.leilei.service.AlarmService;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Future;

/**
 * @author lei
 * @create 2022-03-30 22:45
 * @desc
 **/
@RequestMapping("/alarm")
@RestController
public class AlarmController {
    private final AlarmService alarmService;

    public AlarmController(AlarmService alarmService) {
        this.alarmService = alarmService;
    }

    /**
     * 外部单个请求接口
     * @param id
     * @return
     */
    @GetMapping(value = "/dictionary/{id}")
    public AlarmDictionary requestCollapseAnnotation(@PathVariable Integer id) {
        // 这里注意需要开启Hystrix环境
        try (HystrixRequestContext context = HystrixRequestContext.initializeContext()) {
            Future<AlarmDictionary> f1 = alarmService.find(id);
            return f1.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
