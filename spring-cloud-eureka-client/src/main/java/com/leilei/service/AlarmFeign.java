package com.leilei.service;

import com.leilei.entity.AlarmDictionary;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author lei
 * @create 2022-03-30 22:47
 * @desc
 **/
@FeignClient(value = "alarm-dictionary")
public interface AlarmFeign {
    @PostMapping("/alarm/dictionary/batch")
    List<AlarmDictionary> getBatch(@RequestBody List<Integer> alarms);
}
