package com.leilei.service;

import com.leilei.entity.AlarmDictionary;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.Future;

/**
 * @author lei
 * @create 2022-03-30 22:38
 * @desc
 **/
@Service
public class AlarmService {

    private  final AlarmFeign alarmFeign;

    public AlarmService(AlarmFeign alarmFeign) {
        this.alarmFeign = alarmFeign;
    }

    /**
     * batchMethod 设置批量方法的接口名
     * HystrixProperty name 设置为时间延迟timerDelayInMilliseconds 即多少时间的请求作为一个批量进行提交；value 为时间阈值，单位是毫秒
     * @param id
     * @return
     */
    @HystrixCollapser(batchMethod = "findBatch",
            collapserProperties = {@HystrixProperty(name = "timerDelayInMilliseconds", value = "1000")})
    public Future<AlarmDictionary> find(Integer id) {
        throw new RuntimeException("This method body should not be executed");
    }

    @HystrixCommand
    public List<AlarmDictionary> findBatch(List<Integer> alarms) {
        List<AlarmDictionary> alarmDictionaries = alarmFeign.getBatch(alarms);
        System.out.println(Thread.currentThread().getName() + ":" + alarmDictionaries);
        return alarmDictionaries;
    }
}
