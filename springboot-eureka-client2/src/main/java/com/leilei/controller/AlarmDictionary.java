package com.leilei.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lei
 * @create 2022-03-30 22:40
 * @desc
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlarmDictionary {
    private Integer id;
    private String desc;


}
