package com.leilei.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lei
 * @create 2022-03-30 22:39
 * @desc
 **/
@RestController
@RequestMapping("/alarm/dictionary")
public class AlarmDictionaryController {
    static List<AlarmDictionary> alarms = new ArrayList<>();
    static {
        alarms.add(new AlarmDictionary(1,"808一级报警"));
        alarms.add(new AlarmDictionary(2,"808二级报警"));
        alarms.add(new AlarmDictionary(3,"808三级报警"));
        alarms.add(new AlarmDictionary(4,"疲劳驾驶一级报警"));
    }
    @PostMapping("/batch")
    public List<AlarmDictionary> getBatchAlarmById(@RequestBody List<Integer> alarmIds) {
        return alarms.stream().filter(x -> alarmIds.contains(x.getId())).collect(Collectors.toList());
    }
}
